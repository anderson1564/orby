
//#include <SPI.h>                      //incluye la libreria SPI para la comunicación
//#include <SdFat.h>     
//#include <SdFatUtil.h> //incluye la libreria de la memoria
//#include <SFEMP3Shield.h>             //incluye la libreria del MP3
  //crea la variable para controlar las intrucciones del MP3
//Pins
#define AXIS_X A2
#define AXIS_Y A1
#define PIN_OUT 30
//Times
#define MAX_LOOP_TIME 300
#define FORCED_REDUCE_MAX 3  //Var to forced reduce the max angle
#define TURN_DELAY_TIME 60 
//Vars accelerometer
#define POS_ZERO 340
#define MAX_DIFF 180
#define BUTTTON_INIT 22

//Init states
#define IS_STOP 0
#define IS_READY 1
#define IS_GO 2

//Win states
#define MAX_RANGE 80
#define MIN_RANGE 0
#define WIN_TIME 12
int cont_win_time =0;

typedef unsigned long long int uTime;
uTime dead_time=10000;
uTime dead_time2=dead_time;
//SdFat sd;
//SFEMP3Shield MP3player;
int init_state = IS_STOP;
void setup(){
  //Set the pin mode
  pinMode(PIN_OUT, OUTPUT);
  pinMode(BUTTTON_INIT, INPUT);
  Serial.begin(9600);
}

//Process the the data of the axis
int procAxis(int axis){
  //Get the analog read
  int input = analogRead(axis);
  //Calc the new data base on the axis data
  int result = abs(POS_ZERO-input);
  //TODO: si esto funcuiona volver esto golbal y constante
  int max_angle = MAX_DIFF/FORCED_REDUCE_MAX;
  if(result>max_angle){
    result=max_angle;
  }
  return map(result, 0, max_angle, 0, MAX_LOOP_TIME/2);;
}

void axoDelay(unsigned long long int delay_time){
  //Loop based on the time
  uTime time_end = millis()+delay_time; //Final delay time
  while(millis() < time_end);
}

void blinkOutput(int delay_time){
  //Turn off the output
  digitalWrite(PIN_OUT, LOW);
  axoDelay(delay_time);
  //Turn on the output
  digitalWrite(PIN_OUT, HIGH);
  Serial.print('b');
  axoDelay(delay_time);
}

void loop(){
  int in = digitalRead(BUTTTON_INIT);
  if(init_state!=IS_GO && in){
    if(init_state==IS_STOP){
      init_state=IS_READY;
    }else if(init_state==IS_READY){
      init_state=IS_GO;
      dead_time=millis()+60000;
    }
    axoDelay(300);
  }else if(init_state==IS_GO){
    //Turn off and turn on the output
    blinkOutput(TURN_DELAY_TIME);
    //Get the delay time
    int axis_x = procAxis(AXIS_X);
    int axis_y = procAxis(AXIS_Y);
    uTime delay_time = MAX_LOOP_TIME-(axis_x + axis_y - 5);
    //Delay
    axoDelay(delay_time);
    is_win(axis_x, axis_y);
    if(init_state==IS_STOP){
      
    }else if(millis()>=dead_time){
      Serial.print('l');
      axoDelay(5000);
      init_state = IS_STOP;
    }
  }
}

boolean is_win(int val1, int val2){
  if(val1<=MAX_RANGE && val1>=MIN_RANGE && val2<=MAX_RANGE && val2>=MIN_RANGE){
    ++cont_win_time;
    //if win
    if (cont_win_time>=WIN_TIME){
      init_state=IS_STOP;
      cont_win_time=0;
      Serial.print('w');
      return true;
    }
  }
  else{
    //if get out of the win range
    cont_win_time=0;
  }
  return false;
}